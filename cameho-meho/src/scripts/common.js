let divMoviePoster = document.querySelector(".card");

// regroupage de toutes les promesses en une seule fontion
Promise.all([

    fetch("http://www.omdbapi.com/?i=tt3896198&apikey=b51bdebd&t=back+to+the+future")
    .then(function(response){
        return response.json();
    }),
    fetch("http://www.omdbapi.com/?i=tt3896198&apikey=b51bdebd&t=akira")
    .then(function(response){
        return response.json();
    }),
    fetch("http://www.omdbapi.com/?i=tt3896198&apikey=b51bdebd&t=avengers")
    .then(function(response){
        return response.json();
    }),
    fetch("http://www.omdbapi.com/?i=tt3896198&apikey=b51bdebd&t=ford+v+ferrari")
    .then(function(response){
        return response.json();
    })
])

// fonction traitement de l'objet json
.then ( function(data){
        console.log(data)
    for ( let Movies of data ){
        
        // création des balises HTML qui vont recevoir les données
        let divMovie = document.createElement("div");
        let divMovieInfos = document.createElement("div");
        let h2Title = document.createElement("h2");
        let divRate = document.createElement("span");
        let spanReleased = document.createElement("span");

        // extraction des données voulues
        let poster = Movies.Poster;
        let title = Movies.Title;
        let rate = Movies.Ratings[0].Value
        let released = Movies.Released

        // on attribue des classes et applique du style CSS
        divMovie.className = "movie-poster";
        divMovieInfos.className = "movie-poster-info"
        divMovie.style.backgroundImage = "linear-gradient(0deg, rgba(7,17,27,0.8) 0%, rgba(200,200,200,0.05) 100%), url("+poster+")";
        h2Title.style.fontStyle = "italic";

        //on remplace le contenu des balises par les valeurs recu par l'objet json
        h2Title.innerHTML = title;
        divRate.innerHTML = rate;
        spanReleased.innerHTML = released;

        // on précise la hiérarchie des éléments HTML créés
        divMoviePoster.appendChild(divMovie);
        divMovie.appendChild(divMovieInfos);
        divMovieInfos.appendChild(h2Title);
        divMovieInfos.appendChild(divRate);
        divMovieInfos.appendChild(spanReleased);
    }

});
